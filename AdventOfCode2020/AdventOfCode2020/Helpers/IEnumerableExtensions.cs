﻿using System.Collections.Generic;

namespace AdventOfCode2020.Helpers {
	public static class IEnumerableExtensions {
		public static IEnumerable<T> GetEvery<T>(this IEnumerable<T> source, int increment) {
			int counter = increment;
			using var enumerator = source.GetEnumerator();

			while (enumerator.MoveNext()) {
				if (counter == increment) {
					yield return enumerator.Current;
					counter = 1;
				} else {
					++counter;
				}
			}
		}

		public static IEnumerable<T> ToInfinite<T>(this IEnumerable<T> source) {
			while (true) {
				foreach (var item in source) {
					yield return item;
				}
			}
		}
	}
}
