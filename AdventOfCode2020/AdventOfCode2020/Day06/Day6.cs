﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day6 {
	public static class Day6 {
		public static async Task<int> SolvePartOne() {
			return await GetGroups()
				.Select(g => g.Members.SelectMany(m => m.Answers).Distinct().Count())
				.SumAsync();
		}

		public static async Task<int> SolvePartTwo() {
			var groups = await GetGroups().ToListAsync();

			int allAnswered = 0;

			foreach (var group in groups) {
				var groupAnswers = group.Members.SelectMany(a => a.Answers).GroupBy(a => a);
				allAnswered += groupAnswers.Count(a => a.Count() == group.Members.Count);
			}

			return allAnswered;
		}

		private static async Task<IEnumerable<string>> GetInput() => await File.ReadAllLinesAsync("Day6/Input.txt");

		private static async IAsyncEnumerable<Group> GetGroups() {
			using var input = (await GetInput()).GetEnumerator();

			var group = new List<Person>();

			while (input.MoveNext()) {
				if (input.Current == string.Empty) {
					yield return new Group { Members = group };
					group = new List<Person>();
				} else {
					group.Add(new Person { Answers = input.Current });
				}
			}

			if (group.Any()) {
				yield return new Group { Members = group };
			}
		}

		private class Group {
			public IReadOnlyCollection<Person> Members { get; init; }
		}

		private class Person {
			public string Answers { get; init; }

			public Person() { }

			public Person(string answers) {
				Answers = answers;
			}
		}
	}
}
