﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day7 {
	public static class Day7 {
		public static async Task<int> SolvePartOne() {
			const string target = "shiny gold";
			var rules = await GetBagContents();

			var parents = rules.Keys.Where(k => k != target && BagCanContain(k, target, rules)).ToList();
			return parents.Count();
		}

		public static async Task<int> SolvePartTwo() {
			const string target = "shiny gold";
			var rules = await GetBagContents();
			var cache = new Dictionary<string, int>();

			return requiredChildrenFor(target);

			int requiredChildrenFor(string color) {
				int descendantCount = 0;

				var children = rules[color];
				foreach (var child in children) {
					int requiredChildren = 0;
					if (cache.ContainsKey(child.Color)) {
						requiredChildren = cache[child.Color];
					} else {
						requiredChildren = requiredChildrenFor(child.Color);
						cache[child.Color] = requiredChildren;
					}
					descendantCount += child.Count + (child.Count * requiredChildren);
				}

				return descendantCount;
			}
		}

		private static bool BagCanContain(string start, string target, IDictionary<string, IReadOnlyCollection<BagContents>> rules) {
			var seen = new HashSet<string>();
			var queue = new Queue<string>();
			queue.Enqueue(start);
			seen.Add(start);

			while (queue.Count > 0) {
				var current = queue.Dequeue();
				if (current == target) {
					return true;
				}

				foreach (var contents in rules[current]) {
					if (seen.Add(contents.Color)) {
						queue.Enqueue(contents.Color);
					}
				}
			}

			return false;
		}

		private static async Task<IEnumerable<string>> GetInput() => await File.ReadAllLinesAsync("Day7/Input.txt");

		private static async Task<IDictionary<string, IReadOnlyCollection<BagContents>>> GetBagContents() {
			Regex bagRegex = new Regex(@"^bag[s.,]*$");
			var results = new Dictionary<string, IReadOnlyCollection<BagContents>>();

			var input = await GetInput();
			foreach (var line in input) {
				using var tokens = ((IEnumerable<string>)line.Split()).GetEnumerator();

				var bagColor = new StringBuilder();
				while (tokens.MoveNext() && !bagRegex.IsMatch(tokens.Current)) {
					bagColor.Append(' ');
					bagColor.Append(tokens.Current);
				}

				tokens.MoveNext(); // "contain"

				var contents = new List<BagContents>();
				bool gettingNumber = true;
				int count = 0;
				var contentName = new StringBuilder();
				while (tokens.MoveNext()) {
					if (gettingNumber) {
						if (tokens.Current == "no") {
							while (tokens.MoveNext()) {
								// nothing - just complete iterating so an empty list will get added later
							}
						} else {
							count = int.Parse(tokens.Current);
						}
						gettingNumber = false;
					} else if (bagRegex.IsMatch(tokens.Current)) {
						contents.Add(new BagContents { Color = contentName.ToString().Trim(), Count = count });
						contentName.Clear();
						gettingNumber = true;
					} else {
						contentName.Append(' ');
						contentName.Append(tokens.Current);
					}
				}

				results[bagColor.ToString().Trim()] = contents;
			}

			return results;
		}

		private class BagContents {
			public int Count { get; init; }
			public string Color { get; init; }

			public override string ToString() => $"{Count} {Color}";

			public override bool Equals(object obj) {
				return obj is BagContents other
					&& Count == other.Count
					&& Color == other.Color;
			}

			public override int GetHashCode() => HashCode.Combine(Count, Color);
		}
	}
}
