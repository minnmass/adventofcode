﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day2 {
	public static class Day2 {
		public static async Task<int> SolveDayOne() {
			return (await GetInput()).Count(IsValidPasswordV1);
		}

		public static async Task<int> SolveDayOnePartTwo() {
			return (await GetInput()).Count(IsValidPasswordV2);
		}

		private static bool IsValidPasswordV1(Password password) {
			var count = password.PwString.Count(c => c == password.Required);

			return password.Min <= count && count <= password.Max;
		}

		private static bool IsValidPasswordV2(Password password) {
			return password.PwString[password.Min - 1] == password.Required ^ password.PwString[password.Max - 1] == password.Required;
		}

		private static async Task<IEnumerable<Password>> GetInput() => (await File.ReadAllLinesAsync("Day2/Part1.txt")).Select(i => new Password(i));

		private class Password {
			private static readonly Regex _parseRegex = new Regex(@"^(\d*)-(\d*) (.): (.*)$");

			public int Min { get; }
			public int Max { get; }
			public char Required { get; }
			public string PwString { get; }

			public Password(string input) {
				var matches = _parseRegex.Match(input);

				Min = int.Parse(matches.Groups[1].Value);
				Max = int.Parse(matches.Groups[2].Value);
				Required = matches.Groups[3].Value[0];
				PwString = matches.Groups[4].Value;
			}
		}
	}
}
