﻿using AdventOfCode2020.Day9;
using System;

var timer = System.Diagnostics.Stopwatch.StartNew();
try {
	Console.WriteLine(await Day9.SolvePartTwo());
} catch (Exception ex) {
	Console.WriteLine(ex);
}
timer.Stop();
Console.WriteLine(timer.Elapsed);
