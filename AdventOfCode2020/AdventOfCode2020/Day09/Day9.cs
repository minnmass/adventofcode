﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day9 {
	public static class Day9 {
		public static async Task<long> SolvePartOne() {
			using var enumerator = (await GetInput()).Select(long.Parse).GetEnumerator();
			return new Xmas(25, enumerator).IllegalValues().First();

		}

		public static async Task<long> SolvePartTwo() {
			var numbers = (await GetInput()).Select(long.Parse).ToList();
			using var enumerator = numbers.GetEnumerator();
			var illegalValue = new Xmas(25, enumerator).IllegalValues().First();

			int fromIdx = 0;
			int toIdx = 0;
			long currentSum = 0;
			while (toIdx < numbers.Count) {
				currentSum += numbers[toIdx];
				++toIdx;
				if (currentSum == illegalValue) {
					return FoundValue(numbers, fromIdx, toIdx);
				}
				while (currentSum > illegalValue) {
					currentSum -= numbers[fromIdx];
					++fromIdx;
				}
				if (currentSum == illegalValue) {
					return FoundValue(numbers, fromIdx, toIdx);
				}
			}

			throw new Exception("boom");

			static long FoundValue(List<long> numbers, int fromIdx, int toIdx) {
				long max = long.MinValue;
				long min = long.MaxValue;
				for (int i = fromIdx; i < toIdx; ++i) {
					max = Math.Max(max, numbers[i]);
					min = Math.Min(min, numbers[i]);
				}
				return max + min;
			}
		}

		private static async Task<IEnumerable<string>> GetInput() => await File.ReadAllLinesAsync("Day9/Input.txt");

		private class Xmas {
			private readonly IEnumerator<long> _enumerator;
			private readonly Queue<long> _queue;

			public Xmas(int preambleLength, IEnumerator<long> enumerator) {
				_enumerator = enumerator;

				_queue = new Queue<long>(preambleLength);
				while (_queue.Count < preambleLength && enumerator.MoveNext()) {
					_queue.Enqueue(enumerator.Current);
				}
			}

			public IEnumerable<long> IllegalValues() {
				while (_enumerator.MoveNext()) {
					if (!IsLegal()) {
						yield return _enumerator.Current;
					}
					_queue.Dequeue();
					_queue.Enqueue(_enumerator.Current);
				}

				bool IsLegal() {
					foreach (var i in _queue) {
						foreach (var j in _queue) {
							if (i != j && i + j == _enumerator.Current) {
								return true;
							}
						}
					}
					return false;
				}
			}
		}
	}
}
