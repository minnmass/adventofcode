﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day4 {
	public static class Day4 {
		public static async Task<int> SolvePartOne() {
			var passports = GetPassports();
			return await passports.CountAsync(PassportValidV1);
		}

		public static async Task<int> SolvePartTwo() {
			var passports = GetPassports();

			//var values = passports.Where(PassportValidV2).Select(p => p.PassportId.Length);
			//foreach (var value in await values.Distinct().OrderBy(i => i).ToListAsync()) {
			//	Console.WriteLine(value);
			//}
			//return 7;

			return await passports.CountAsync(PassportValidV2);
		}

		private static bool PassportValidV1(Passport passport) {
			return !(
				string.IsNullOrWhiteSpace(passport.BirthYear) ||
				string.IsNullOrWhiteSpace(passport.ExpirationYear) ||
				string.IsNullOrWhiteSpace(passport.EyeColor) ||
				string.IsNullOrWhiteSpace(passport.HairColor) ||
				string.IsNullOrWhiteSpace(passport.Height) ||
				string.IsNullOrWhiteSpace(passport.IssueYear) ||
				string.IsNullOrWhiteSpace(passport.PassportId)
			);
		}

		private static readonly Regex _heightRegex = new Regex(@"^(\d*)(cm|in)$");
		private static readonly Regex _hairColorRegex = new Regex(@"^#[0123456789abcdef]{6}$");
		private static readonly IReadOnlyCollection<string> _validEyeColors = new[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" };
		private static readonly Regex _passportIdRegex = new Regex(@"^\d{9}$");

		private static bool PassportValidV2(Passport passport) {
			bool valid = PassportValidV1(passport);

			valid = valid && int.TryParse(passport.BirthYear, out var birthYear) && 1920 <= birthYear && birthYear <= 2002;
			valid = valid && int.TryParse(passport.IssueYear, out var issueYear) && 2010 <= issueYear && issueYear <= 2020;
			valid = valid && int.TryParse(passport.ExpirationYear, out var expirationYear) && 2020 <= expirationYear && expirationYear <= 2030;
			valid = valid && ValidHeight();
			valid = valid && _hairColorRegex.IsMatch(passport.HairColor);
			valid = valid && _validEyeColors.Contains(passport.EyeColor);
			valid = valid && _passportIdRegex.IsMatch(passport.PassportId);

			return valid;

			bool ValidHeight() {
				var matches = _heightRegex.Matches(passport.Height);

				var dataChunks = matches.FirstOrDefault();
				if (dataChunks is null) {
					return false;
				}

				if (int.TryParse(dataChunks.Groups[1].Value, out int numeric)) {
					var units = dataChunks.Groups[2].Value;

					if (units == "cm") {
						return 150 <= numeric && numeric <= 193;
					}
					if (units == "in") {
						return 59 <= numeric && numeric <= 76;
					}
				}

				return false;
			}
		}

		private static async Task<IEnumerable<string>> GetInput() => await File.ReadAllLinesAsync("Day4/Input.txt");

		private static async IAsyncEnumerable<Passport> GetPassports() {
			using var tokens = (await GetInput()).Select(i => i.Split()).SelectMany(i => i).GetEnumerator();

			do {
				yield return new Passport(tokens);
			} while (tokens.Current != null);
		}

		private class Passport {
			public string BirthYear { get; }
			public string IssueYear { get; }
			public string ExpirationYear { get; }
			public string Height { get; }
			public string HairColor { get; }
			public string EyeColor { get; }
			public string PassportId { get; }
			public string CountryId { get; }

			public Passport(IEnumerator<string> tokens) {
				while (tokens.MoveNext() && tokens.Current != string.Empty) {
					var data = tokens.Current.Split(':');
					switch (data[0]) {
						case "byr":
							BirthYear = data[1];
							break;
						case "iyr":
							IssueYear = data[1];
							break;
						case "eyr":
							ExpirationYear = data[1];
							break;
						case "hgt":
							Height = data[1];
							break;
						case "hcl":
							HairColor = data[1];
							break;
						case "ecl":
							EyeColor = data[1];
							break;
						case "pid":
							PassportId = data[1];
							break;
						case "cid":
							CountryId = data[1];
							break;
					}
				}
			}
		}
	}
}
