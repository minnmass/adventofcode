﻿using AdventOfCode2020.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day3 {
	public static class Day3 {
		private const char Tree = '#';

		public static async Task<long> SolvePartOne() {
			return CountTrees(1, 3, await GetInput());
		}

		public static async Task<long> SolvePartTwo() {
			var input = (await GetInput()).ToList();

			long treeProduct = 1;

			foreach (var right in new[] { 1, 3, 5, 7 }) {
				treeProduct *= CountTrees(1, right, input);
			}
			treeProduct *= CountTrees(2, 1, input);

			return treeProduct;
		}

		private static long CountTrees(int down, int right, IEnumerable<string> input) {
			int current = 0;
			long trees = 0;

			foreach (var row in input.GetEvery(down).ToList()) {
				if (row[current] == Tree) {
					++trees;
				}
				current = (current + right) % row.Length;
			}

			return trees;
		}

		private static async Task<IEnumerable<string>> GetInput() => await File.ReadAllLinesAsync("Day3/Input.txt");
	}
}
