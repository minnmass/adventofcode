﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day8 {
	public static class Day8 {
		public static async Task<int> SolvePartOne() {
			var stateMachine = await GetStateMachine();

			RunToCompletionOrLoop(stateMachine);

			return stateMachine.Accumulator;
		}

		public static async Task<int> SolvePartTwo() {
			var instructions = await GetInstructions();

			for (int i = 0; i < instructions.Count; ++i) {
				IInstruction replacementInstruction = null;
				switch (instructions[i]) {
					case NoOp n:
						replacementInstruction = new Jump(n.Value);
						break;
					case Jump j:
						replacementInstruction = new NoOp(j.Distance);
						break;
				}
				if (replacementInstruction is not null) {
					var newInstructions = CloneAndModify(i, replacementInstruction);
					var machine = new StateMachine { Instructions = newInstructions };
					RunToCompletionOrLoop(machine);
					if (machine.Done) {
						return machine.Accumulator;
					}
				}
			}

			throw new Exception("No valid replacement instruction found.");

			IReadOnlyList<IInstruction> CloneAndModify(int index, IInstruction newInstruction) {
				var newInstructions = new List<IInstruction>(instructions);
				newInstructions[index] = newInstruction;
				return newInstructions;
			}
		}

		private static void RunToCompletionOrLoop(StateMachine stateMachine) {
			var seen = new HashSet<int>();
			while (!stateMachine.Done && seen.Add(stateMachine.InstructionPointer)) {
				stateMachine.Step();
			}
		}

		private static async Task<IEnumerable<string>> GetInput() => await File.ReadAllLinesAsync("Day8/Input.txt");

		private static async Task<IList<IInstruction>> GetInstructions() => (await GetInput()).Select(InstructionFactory.GetInstruction).ToList();

		private static async Task<StateMachine> GetStateMachine(IReadOnlyList<IInstruction> instructions = null) {
			return new StateMachine {
				Instructions = instructions ?? (await GetInstructions()) as IReadOnlyList<IInstruction>
			};
		}
	}

	public class StateMachine {
		private int accumulator = 0;
		private int instructionPointer = 0;

		public int Accumulator { get => accumulator; }
		public int InstructionPointer { get => instructionPointer; }
		public bool Done { get; private set; } = false;
		public bool DoneOnInstructionExactlyPastListEnd { get; private set; } = false;

		public IReadOnlyList<IInstruction> Instructions { get; init; }

		public void Step() {
			Instructions[instructionPointer].Step(ref accumulator, ref instructionPointer);
			if (instructionPointer >= Instructions.Count) {
				Done = true;
				if (instructionPointer == Instructions.Count) {
					DoneOnInstructionExactlyPastListEnd = true;
				}
			}
		}
	}

	public static class InstructionFactory {
		public static IInstruction GetInstruction(string input) {
			var chunks = input.Split();
			var name = chunks[0];
			if (!int.TryParse(chunks[1], out int value)) {
				throw new Exception("boom");
			}
			// var value = int.Parse(chunks[1]);


			return name switch {
				"nop" => new NoOp(value),
				"acc" => new Accumulate(value),
				"jmp" => new Jump(value),
				_ => throw new ArgumentException($"Invalid input: {input}"),
			};
		}
	}

	public interface IInstruction {
		void Step(ref int Accumulator, ref int InstructionPointer);
	}

	public class NoOp : IInstruction {
		public int Value { get; }

		public void Step(ref int Accumulator, ref int InstructionPointer) {
			++InstructionPointer;
		}

		public NoOp(int value) {
			Value = value;
		}
	}

	public class Accumulate : IInstruction {
		private int accumulatorChange;

		public void Step(ref int Accumulator, ref int InstructionPointer) {
			++InstructionPointer;
			Accumulator += accumulatorChange;
		}

		public Accumulate(int amount) {
			accumulatorChange = amount;
		}
	}

	public class Jump : IInstruction {
		public int Distance { get; }

		public void Step(ref int Accumulator, ref int InstructionPointer) {
			InstructionPointer += Distance;
		}

		public Jump(int distance) {
			Distance = distance;
		}
	}
}
