﻿using AdventOfCode2020.Helpers;
using MissingLinq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day5 {
	public static class Day5 {
		public static async Task<int> SolvePartOne() {
			var seats = (await GetInput()).Select(i => new Seat(i));

			var maxSeat = seats.MaxBy(s => s.Id);

			return maxSeat.Id;
		}

		public static async Task<int> SolvePartTwo() {
			var seatIds = (await GetInput()).Select(i => new Seat(i).Id).OrderBy(i => i).ToList();

			for (int i = 1; i < seatIds.Count; ++i) {
				var lastSeat = seatIds[i - 1];
				var thisSeat = seatIds[i];
				var expected = lastSeat + 1;
				if (thisSeat != expected) {
					return expected;
				}
			}

			return -1;
		}

		private static async Task<IEnumerable<string>> GetInput() => await File.ReadAllLinesAsync("Day5/Input.txt");

		private class Seat {
			private const int _rows = 128;
			private const int _columns = 8;

			private static readonly IReadOnlyList<BitArray> _fArrays;
			private static readonly IReadOnlyList<BitArray> _bArrays;
			private static readonly IReadOnlyList<BitArray> _lArrays;
			private static readonly IReadOnlyList<BitArray> _rArrays;

			static Seat() {
				int on = _rows / 2;
				var fArrays = new List<BitArray>();
				while (on > 0) {
					fArrays.Add(new BitArray(Enumerable.Repeat(true, on).Concat(Enumerable.Repeat(false, on)).ToInfinite().Take(_rows).ToArray()));
					on /= 2;
				}
				_fArrays = fArrays;
				_bArrays = fArrays.Select(a => new BitArray(a)).ToList();
				foreach (var array in _bArrays) {
					array.Not();
				}

				on = _columns / 2;
				var lArrays = new List<BitArray>();
				while (on > 0) {
					lArrays.Add(new BitArray(Enumerable.Repeat(true, on).Concat(Enumerable.Repeat(false, on)).ToInfinite().Take(_columns).ToArray()));
					on /= 2;
				}
				_lArrays = lArrays;
				_rArrays = lArrays.Select(a => new BitArray(a)).ToList();
				foreach (var array in _rArrays) {
					array.Not();
				}
			}

			public int Row { get; }
			public int Column { get; }
			public int Id { get; }

			public Seat(string code) {
				var rowField = new BitArray(_rows, true);

				for (int i = 0; i < _fArrays.Count; ++i) {
					rowField.And(code[i] == 'F' ? _fArrays[i] : _bArrays[i]);
				}
				while (!rowField[Row]) {
					++Row;
				}

				var colField = new BitArray(_columns, true);
				for (int i = 0; i < _lArrays.Count; ++i) {
					colField.And(code[i + _fArrays.Count] == 'L' ? _lArrays[i] : _rArrays[i]);
				}
				while (!colField[Column]) {
					++Column;
				}

				Id = (Row * 8) + Column;
			}
		}
	}
}
