﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2020.Day1 {
	public static class Day1 {
		public static async Task<int> SolveAsync() {
			const int targetSum = 2020;

			var input = await GetInput();
			var partialSolution = GetMatchingNumber(targetSum, input).Value;

			return partialSolution * (targetSum - partialSolution);
		}

		public static async Task<int> SolvePartTwoAsync() {
			const int targetSum = 2020;
			var input = await GetInput();

			foreach (var a in input) {
				foreach (var b in input.Where(i => i != a)) {
					foreach (var c in input.Where(i => i != a && i != b)) {
						if (a + b + c == targetSum) {
							return a * b * c;
						}
					}
				}
			}

			throw new ArgumentException();
		}

		private static async Task<IReadOnlyCollection<int>> GetInput() => (await File.ReadAllLinesAsync("Day1/Part1.txt")).Select(i => int.Parse(i)).ToList();

		private static int? GetMatchingNumber(int target, IReadOnlyCollection<int> input) {
			var compliments = input.Select(i => target - i);

			var partialSolution = compliments.FirstOrDefault(i => input.Contains(i));

			return partialSolution;
		}
	}
}
