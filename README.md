https://adventofcode.com/

Each year is in its own solution, each day is in its own class (with my specific input).

Code is intended to be well-designed, with the caveats that each day acts as a mini-program (ie., the "solve" method acts largely like a "main" method) and that things like file input paths or validation parameters are hard-coded where they might typically be config-based.