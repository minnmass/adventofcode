﻿namespace AdventOfCode2023 {
	public abstract class Day {
		private readonly bool _test;

		public Day(bool test) {
			_test = test;
		}

		public abstract Task<string> SolvePart1();
		public abstract Task<string> SolvePart2();

		internal Task<string> GetInput(bool part1) {
			return File.ReadAllTextAsync(GetRelativePath(part1));
		}

		internal IAsyncEnumerable<string> GetInputLines(bool part1) {
			return File.ReadLinesAsync(GetRelativePath(part1));
		}

		private string GetRelativePath(bool part1) {
			var className = this.GetType().Name;

			string middlePart = _test
				? part1
					? "test"
					: "test.part2"
				: "input";

			var fileName = $"{className}.{middlePart}.txt";
			var relativePath = Path.Combine("Inputs", fileName);
			if (Path.Exists(relativePath)) {
				return relativePath;
			}
			if (_test && !part1) {
				return GetRelativePath(true);
			}
			throw new InvalidOperationException();
		}
	}
}
