﻿namespace AdventOfCode2023 {
	public class Day02(bool test) : Day(test) {
		public override async Task<string> SolvePart1() {
			var lines = GetInputLines(part1: true);

			var maxCubeCount = new CubeCount {
				Red = 12,
				Green = 13,
				Blue = 14,
			};

			int validGameSum = 0;
			await foreach (var line in lines) {
				var game = ParseLine(line);

				if (GameIsValid(game)) {
					validGameSum += game.Id;
				}
			}
			return validGameSum.ToString();

			bool GameIsValid(Game game) {
				return game.CubeCounts.All(g =>
					g.Red <= maxCubeCount.Red
					&& g.Blue <= maxCubeCount.Blue
					&& g.Green <= maxCubeCount.Green
				);
			}
		}

		public override async Task<string> SolvePart2() {
			var lines = GetInputLines(part1: false);

			int powerSum = 0;
			await foreach (var line in lines) {
				var game = ParseLine(line);

				int minRed = 0;
				int minBlue = 0;
				int minGreen = 0;

				foreach (var count in game.CubeCounts) {
					minRed = Math.Max(minRed, count.Red);
					minBlue = Math.Max(minBlue, count.Blue);
					minGreen = Math.Max(minGreen, count.Green);
				}

				int power = minRed * minBlue * minGreen;
				powerSum += power;
			}

			return powerSum.ToString();
		}

		public static Game ParseLine(string line) {
			var colonSplit = line.Split(':');
			var gameNumber = int.Parse(colonSplit[0].Split(' ')[1]);

			var games = colonSplit[1].Split(';')
				.Select(
					gameChunk => {
						var stringChunks = gameChunk.Split(',');
						int redCount = 0;
						int greenCount = 0;
						int blueCount = 0;
						foreach (var count in stringChunks) {
							var countChunks = count.Split(' ', StringSplitOptions.RemoveEmptyEntries);
							switch (countChunks[1]) {
								case "red":
									redCount = int.Parse(countChunks[0]);
									break;
								case "blue":
									blueCount = int.Parse(countChunks[0]);
									break;
								case "green":
									greenCount = int.Parse(countChunks[0]);
									break;
							}
						}
						return new CubeCount {
							Red = redCount,
							Blue = blueCount,
							Green = greenCount,
						};
					}
				);

			return new Game {
				Id = gameNumber,
				CubeCounts = games.ToArray(),
			};
		}

		public record Game {
			public int Id { get; init; }
			public IReadOnlyCollection<CubeCount> CubeCounts { get; init; } = Array.Empty<CubeCount>();
		}

		public record CubeCount {
			public int Red { get; init; }
			public int Green { get; init; }
			public int Blue { get; init; }
		}
	}
}
