﻿
namespace AdventOfCode2023.Extensions {
	public static class IAsyncEnumerableExtensions {
		public static async IAsyncEnumerable<TOut> Select<TIn, TOut>(this IAsyncEnumerable<TIn> source, Func<TIn, TOut> translator) {
			await foreach (var item in source) {
				yield return translator.Invoke(item);
			}
		}

		public static async Task<ulong> Sum(this IAsyncEnumerable<ulong> source) {
			ulong sum = 0;
			await foreach (var item in source) {
				checked {
					sum += item;
				}
			}
			return sum;
		}
	}
}
