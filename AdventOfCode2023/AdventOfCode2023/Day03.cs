﻿using System.Text.RegularExpressions;

namespace AdventOfCode2023 {
	internal class Day03(bool test) : Day(test) {
		public override async Task<string> SolvePart1() {
			var input = await GetInput(part1: true);
			var partNumbers = GetPartNumbers(input);
			return partNumbers.Sum().ToString();

			static IEnumerable<int> GetPartNumbers(string input) {
				var lines = input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
				int parsed;

				for (int lineIdx = 0; lineIdx < lines.Length; ++lineIdx) {
					var line = lines[lineIdx];
					string current = String.Empty;

					for (int charIdx = 0; charIdx < line.Length; ++charIdx) {
						if (char.IsDigit(line[charIdx])) {
							current += line[charIdx];
						} else {
							if (IsPartNumber(lineIdx, current, charIdx, out parsed)) {
								yield return parsed;
							}
							current = String.Empty;
						}
					}

					if (IsPartNumber(lineIdx, current, line.Length, out parsed)) {
						yield return parsed;
					}
				}

				bool IsPartNumber(int lineIdx, string possibleNumber, int endIdx, out int parsed) {
					if (!int.TryParse(possibleNumber, out parsed)) {
						return false;
					}

					int startIdx = endIdx - possibleNumber.Length;
					++endIdx;
					--startIdx;

					string surrounding = String.Empty;

					for (int checkLineOffset = -1; checkLineOffset < 2; ++checkLineOffset) {
						var checkLineIndex = lineIdx + checkLineOffset;
						if (checkLineIndex >= 0 && checkLineIndex < lines.Length) {
							var checkLine = lines[checkLineIndex];
							for (int checkCharIdx = startIdx; checkCharIdx < endIdx; ++checkCharIdx) {
								if (checkCharIdx >= 0 && checkCharIdx < checkLine.Length) {
									char checkChar = checkLine[checkCharIdx];
									surrounding += checkChar;
									if (checkChar != '.' && !char.IsDigit(checkChar)) {
										//return true;
									}
								}
							}
						}
						surrounding += Environment.NewLine;
					}
					//return false;
					bool isValid = surrounding.Any(c => !(char.IsDigit(c) || char.IsWhiteSpace(c) || c == '.'));
					/*
					Console.WriteLine(parsed);
					Console.ForegroundColor = isValid ? ConsoleColor.Green : ConsoleColor.Red;
					Console.Write(surrounding);
					Console.ForegroundColor = ConsoleColor.Blue;
					Console.WriteLine("---------------------------------------------");
					Console.ResetColor();
					*/
					return isValid;
				}
			}
		}

		public override async Task<string> SolvePart2() {
			var input = await GetInput(part1: false);
			var gearRatios = GetGearRatios(input);
			return gearRatios.Sum().ToString();

			static IEnumerable<int> GetGearRatios(string input) {
				var lines = input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);

				for (int lineIdx = 0; lineIdx < lines.Length; ++lineIdx) {
					var line = lines[lineIdx];
					for (int charIdx = 0; charIdx < line.Length; ++charIdx) {
						var current = line[charIdx];
						if (current == '*') {
							var adjacentNumbers = GetAdjacentNumbers(lineIdx, charIdx).Take(3).ToArray();
							if (adjacentNumbers.Length == 2) {
								yield return adjacentNumbers[0] * adjacentNumbers[1];
							}
						}
					}
				}

				IEnumerable<int> GetAdjacentNumbers(int lineIdx, int charIdx) {
					var fragment = String.Empty;
					for (int lineOffset = -1; lineOffset < 2; ++lineOffset) {
						var checkLineIndex = lineIdx + lineOffset;
						if (checkLineIndex >= 0 && checkLineIndex < lines.Length) {
							var checkLine = lines[checkLineIndex];
							string lineChunk = checkLine[charIdx].ToString();
							int checkCharIdx = charIdx - 1;

							while (checkCharIdx >= 0 && char.IsDigit(checkLine[checkCharIdx])) {
								lineChunk = checkLine[checkCharIdx] + lineChunk;
								--checkCharIdx;
							}

							checkCharIdx = charIdx + 1;
							while (checkCharIdx < checkLine.Length && char.IsDigit(checkLine[checkCharIdx])) {
								lineChunk += checkLine[checkCharIdx];
								++checkCharIdx;
							}

							fragment += lineChunk;
							fragment += Environment.NewLine;
						}
					}
					var matches = Regex.Matches(fragment, "[0-9]+");
					return matches
						.Select(m => m.Value)
						.Select(int.Parse);
				}
			}
		}
	}
}
