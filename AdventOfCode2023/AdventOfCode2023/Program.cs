﻿using AdventOfCode2023;

await Test(typeof(Day01), "142", "54644", "281", "53348");
await Test(typeof(Day02), "8", "2331", "2286", "71585");
await Test(typeof(Day03), "4361", "536576", "467835", "75741499");
await Test(typeof(Day04), "13", "15205", "30", "");

static async Task Test(Type t, string part1ExpectedTest, string part1ExpectedReal, string part2ExpectedTest, string part2ExpectedReal) {
	await InnerTestAsync(true, true, part1ExpectedTest);
	await InnerTestAsync(false, true, part1ExpectedReal);

	await InnerTestAsync(true, false, part2ExpectedTest);
	await InnerTestAsync(false, false, part2ExpectedReal);

	Console.WriteLine();

	async Task InnerTestAsync(bool test, bool part1, string expected) {
		var timer = System.Diagnostics.Stopwatch.StartNew();
		if (Activator.CreateInstance(t, args: new object[] { test }) is not Day day) {
			throw new ArgumentNullException(nameof(day));
		}

		var result = await (part1 ? day.SolvePart1() : day.SolvePart2());
		timer.Stop();
		bool correct = String.Equals(expected, result);
		if (correct) {
			Console.WriteLine($"Day {t.Name} : part1 {part1} : test {test} : {timer.Elapsed}");
		} else {
			Console.Error.WriteLine($"ERROR: Day {t.Name} : part1 {part1} : test {test} : {timer.Elapsed} : got <<{result}>> expected <<{expected}>>");
		}
	}
}
