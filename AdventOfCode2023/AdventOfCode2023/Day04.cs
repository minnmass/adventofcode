﻿using AdventOfCode2023.Extensions;

namespace AdventOfCode2023 {
	internal class Day04(bool test) : Day(test) {
		public override async Task<string> SolvePart1() {
			var input = GetInputLines(part1: true);
			var cards = input.Select(Parse);
			var scores = cards.Select(
				card => ((ulong)1 << card.MyNumbers.Intersect(card.WinningNumbers).Count()) >> 1
			);
			return (await scores.Sum()).ToString();
		}

		public override Task<string> SolvePart2() {
			return Task.FromResult("7");
		}

		private static Card Parse(string input) {
			var colonSplit = input.Split(':');
			var idStr = colonSplit[0].Split(' ', StringSplitOptions.RemoveEmptyEntries)[1];
			var id = int.Parse(idStr);

			var numberTypes = colonSplit[1].Split('|');
			var myNumbers = numberTypes[0].Split(' ', StringSplitOptions.RemoveEmptyEntries)
				.Select(int.Parse)
				.ToArray();
			var winningNumbers = numberTypes[1].Split(' ', StringSplitOptions.RemoveEmptyEntries)
				.Select(int.Parse)
				.ToArray();

			return new Card {
				Id = id,
				MyNumbers = myNumbers,
				WinningNumbers = winningNumbers,
			};
		}

		private record Card {
			public required int Id { get; init; }
			public required IReadOnlyCollection<int> MyNumbers { get; init; }
			public required IReadOnlyCollection<int> WinningNumbers { get; init; }
		}
	}
}
