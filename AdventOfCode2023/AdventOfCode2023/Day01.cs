﻿namespace AdventOfCode2023 {
	public class Day01(bool test) : Day(test) {
		private static readonly IReadOnlyDictionary<string, int> _digitValues;
		private static readonly IReadOnlyDictionary<string, int> _wordValues;

		static Day01() {
			var digits = Enumerable.Range(0, 10)
				.ToDictionary(i => i.ToString(), i => i);
			_digitValues = digits;
			var words = new Dictionary<string, int>(digits);
			words["one"] = 1;
			words["two"] = 2;
			words["three"] = 3;
			words["four"] = 4;
			words["five"] = 5;
			words["six"] = 6;
			words["seven"] = 7;
			words["eight"] = 8;
			words["nine"] = 9;
			_wordValues = words;
		}

		public override Task<string> SolvePart1() {
			var input = GetInputLines(part1: true);
			return SolveAsync(_digitValues, input);
		}

		public override Task<string> SolvePart2() {
			var input = GetInputLines(part1: false);
			return SolveAsync(_wordValues, input);
		}

		private static async Task<string> SolveAsync(IReadOnlyDictionary<string, int> dict, IAsyncEnumerable<string> lines) {
			int sum = 0;
			await foreach (var line in lines) {
				var firstNumber = dict.MinBy(
					kvp => {
						var idx = line.IndexOf(kvp.Key);
						return idx < 0
							? int.MaxValue
							: idx;
					}
				).Value;
				var lastNumber = dict.MaxBy(kvp => line.LastIndexOf(kvp.Key)).Value;

				var val = (10 * firstNumber) + lastNumber;
				sum += val;
			}
			return sum.ToString();
		}
	}
}
